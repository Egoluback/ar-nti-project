﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour
{
    public int[,] Pyramids = { { 2, 5, 15, 0 }, { 2, -5, 15, 0 }, {2, 5, -15, 0 }, { 2, -5, -15, 0 }, { 2, 15, 5, 0 }, { 2, 15, -5, 0 }, { 2, -15, 5, 0 }, { 2, -15, -5, 0 }, { 2, 35, 35, 0 }, {2, -35, -35, 0 } };
    public float x = 5;
    public float z = 0;
    public int velocity_x = 1;
    public int velocity_z = 1;
    public bool ground = true;
    public int remain = 3;
    public int round = 10;
    public GameObject cube;
    // Start is called before the first frame update
    void Start()
    {
        /*for (int i = 0; i < 10; ++i)
        {
            Instantiate(cube,new Vector3(Pyramids[i,1],0,Pyramids[i,2]),transform.rotation);
        }*/
    }

    // Update is called once per frame
    void Update()
    {
        for (int i = 0; i < 10; ++i)
        {
            Detect(Pyramids[i, 0], Pyramids[i, 1], Pyramids[i, 2], Pyramids[i, 3]);
        }
        float vx = velocity_x * Time.deltaTime * 20;
        float vz = velocity_z * Time.deltaTime * 20;
        x += vx;
        if (x > 40)
        {
            velocity_x = -1;
            x = 40;
            z = Round(z);
        }
        if (x < -40)
        {
            velocity_x = 1;
            x = -40;
            z = Round(z);
        }
        z += vz;
        if (z > 50)
        {
            velocity_z = -1;
            z = 50;
            x = Round(x);
        }
        if (z < -50)
        {
            velocity_z = 1;
            z = -50;
            x = Round(x);
        }
        transform.SetPositionAndRotation(new Vector3(x,0,z),transform.rotation);

    }
    void Detect(int type, int px, int pz, int pside)
    {
        int upper = px + 5;
        int lower = px - 5;
        int left = pz - 5;
        int right = pz + 5;
        if(x>lower&&x<upper&&z>left&&z<right)
        {
            Debug.Log(x);
            Debug.Log(px);
            float h = x - px;
            float w = z - pz;
            if (System.Math.Abs(h) > System.Math.Abs(w))
                React(px,pz,type,3 + (int)(h / System.Math.Abs(h)),pside);
            else
                React(px,pz,type,2 + (int)(w / System.Math.Abs(w)),pside);
        }
    }
    void React(int px,int pz,int type,int side, int pside)
    {
        if (type == 1)
        {
            Reflect(x,z,-velocity_x,-velocity_z,true);
        }
        else if (type == 2)
        {
            switch (side)
            {
                case 1:Reflect(px,pz+5,-velocity_x,1);break;
                case 2: Reflect(px+5, pz, 1, -velocity_z); break;
                case 3: Reflect(px, pz - 5, -velocity_x, -1); break;
                case 4: Reflect(px-5, pz, -1, -velocity_z); break;
            }
        }
        else if (type == 3)
        {
            switch (side)
            {
                case 1: Reflect(px, pz - 5, 0, -1); break;
                case 2: Reflect(px-5, pz, -1, 0); break;
                case 3: Reflect(px, pz + 5, 0, 1); break;
                case 4: Reflect(px+5, pz, 1,0); break;

            }
        }
        else if (type == 4)
        {
            switch (pside)
            {
                case 1: Reflect(px, pz - 5, -1,-1); break;
                case 2: Reflect(px-5, pz, -1, 1); break;
                case 3: Reflect(px, pz + 5, 1, 1); break;
                case 4: Reflect(px+5, pz, 1, -1); break;
            }
        }
        else
        {
            switch (side)
            {
                case 1: Reflect(px, pz - 5, velocity_x, -1, true); break;
                case 2: Reflect(px - 5, pz, -1, velocity_z, true); break;
                case 3: Reflect(px, pz + 5, velocity_x, 1, true); break;
                case 4: Reflect(px + 5, pz, 1, velocity_z, true); break;
            }
        }
    }
    void Reflect(float ax, float az, int vx, int vz, bool high=false)
    {
        if (high || ground)
        {
            x = ax;
            z = az;
            velocity_x = vx;
            velocity_z = vz;
            remain--;
            round--;
            if (remain == 0)
            {
                ground = !ground;
                remain = 3;
            }
        }
    }
    float Round(float num)
    {
        int s = 5;
        if (num < 0) s = -5;
        num = ((int)num/10*10)+s;
        return num;
    }
}
